import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

const FooterBar = props => {
    return (
        <View>
            <View style={styles.viewStyle}>
                <TouchableOpacity style={styles.cardText} onPress={() => props.navigation.navigate('ActivityScreen')}>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={styles.textStyle}>ไปกับเพื่อน</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.cardText} onPress={() => props.navigation.navigate('CategoryScreen')} >
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={styles.textStyle}>ทริปรอบๆ</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = {
    viewStyle: {
        height: 60,
        flexDirection: 'row',
        backgroundColor: '#2a3132',
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        elevation: 2,
        position: 'relative'
    },
    textStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        fontSize: 20,
        fontWeight: 'bold',
        color: '#fff'
    },
    cardText: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 0.5,
        borderColor: 'gray',
    }
}

export default FooterBar;