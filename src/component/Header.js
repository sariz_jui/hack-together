import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import FIcon from 'react-native-vector-icons/FontAwesome';

const Header = props => {
    return (
        <View style={styles.viewStyle}>
            <View style={{ flex: 1 }}></View>
            <View style={{ flex: 5, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={styles.textStyle}>{props.name}</Text>
            </View>
            <TouchableOpacity style={{ flex: 1 }} onPress={() => props.navigation.navigate('ProfileScreen')} >
                <View style={{ width: 25, height: 25, borderRadius: 25 / 2, borderWidth: 4, borderColor: '#333333', alignItems: 'center', justifyContent: 'center' }}>
                    <FIcon name="user" color="#333333" size={20} />
                </View>
            </TouchableOpacity>
        </View >
    );
}

const styles = {
    viewStyle: {
        height: 60,
        backgroundColor: '#336b87',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        elevation: 2,
        position: 'relative'
    },
    textStyle: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#fff'
    }
}

export default Header;