import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const Button = (props) => {
    return (
        <TouchableOpacity
            onPress={props.onPress}
            style={styles.buttonStyle}
            
        >
            <Text style={styles.textStyle}>{props.name}</Text>
        </TouchableOpacity>
    )
}

const styles = {
    buttonStyle: {
        flex: 1,
        alignSelf: 'stretch',
        backgroundColor: '#fff',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: 'black',
        margin: 2,
        justifyContent: 'center',
        alignItems: 'center',

    },
    textStyle: {
        color: '#007aff',
        fontSize: 16,
        paddingTop: 10,
        paddingBottom: 10
    }
}
export default Button;