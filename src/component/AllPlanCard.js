import React from 'react';
import { View, Text, Image, Linking, ScrollView, ImageBackground } from 'react-native';
import Button from './Button';


const AllPlanCard = props => {
    return (

        <View style={styles.containerStyle}>
            <View style={styles.container}>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={require('../images/imageBG.jpg')} style={styles.imageStyle} />
                </View>

                <View style={styles.cardTextStyle}>
                    <View style={{ flex: 2 }}>
                        <View>
                            <Text style={styles.textHeader}>แผนเที่ยวเชียงใหม่{props.name}</Text>
                            <View style={{ flex: 1, backgroundColor: 'gray', borderRadius: 10 }}></View>
                        </View>

                        <Text>วันที่เริ่ม : {props.date}</Text>
                        <Text>ระยะเวลา {props.duration} วัน</Text>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-around' }}>
                        <Button color="green" name="รายละเอียด"
                            onPress={() => props.navigation.navigate('CreateActivityScreen')} />
                        <Button name="เข้าร่วม"
                            onPress={() => props.navigation.navigate('PlanDetail')} />
                    </View>
                </View>
            </View>
        </View>
    );
}
const styles = {
    containerStyle: {
        height: 150,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        elevation: 2,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10
    },
    container: {
        height: 130,
        flex: 1,
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderRadius: 5,
        padding: 5,
        backgroundColor: '#fff',
        justifyContent: 'flex-start',
        borderColor: '#ddd',
        position: 'relative'
    },
    imageStyle: {
        flex: 2,
        width: 100,
        height: 100,
        borderRadius: 2,
        margin: 5,
    },
    cardTextStyle: {
        flex: 3,
        justifyContent: 'space-around',
        margin: 10,
    },
    textHeader: {
        fontSize: 20,
        fontWeight: 'bold',
    }

}

export default AllPlanCard;