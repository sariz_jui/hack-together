import React from 'react';

import { View, Text, TouchableOpacity, ImageBackground } from 'react-native';


const CardCategory = (props) => {
    return (
        <View>
            <View style={styles.cardContainer}>
                <ImageBackground source={require('../images/imageBG.jpg')} style={styles.cardCategory}>
                    <Text style={styles.textCategory}>สายบุญ</Text>
                </ImageBackground>
            </View>
            <View style={styles.cardContainer}>
                <ImageBackground source={require('../images/imageBG.jpg')} style={styles.cardCategory}>
                    <Text style={styles.textCategory}>สายลุย</Text>
                </ImageBackground>
            </View>
            <View style={styles.cardContainer}>
                <ImageBackground source={require('../images/imageBG.jpg')} style={styles.cardCategory}>
                    <Text style={styles.textCategory}>สายชิว</Text>
                </ImageBackground>
            </View>
        </View>
    );
}

const styles = {
    cardContainer: {
        borderWidth: 1,
        borderRadius: 2,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        elevation: 2,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10
    },
    cardCategory: {
        flexDirection: 'column',
        //space-around ให้พื้นที่ข้างบนล่างเท่าๆกัน space-between ให้ช่องว่างตรงกลาง
        justifyContent: 'space-around',
        alignItems: 'center',
        borderRadius: 20,
        height: 200,

    },
    textCategory: {
        fontSize: 40,
        fontWeight: 'bold',
        color: 'white'

    },
}

export default CardCategory;