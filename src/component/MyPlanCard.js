import React from 'react';
import { View, Text, Image, Linking, ScrollView, ImageBackground } from 'react-native';
import Button from './Button';


const MyPlanCard = (props) => {
    return (

        <View style={styles.containerStyle}>
            <View style={styles.container}>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={require('../images/imageBG.jpg')} style={styles.imageStyle} />
                </View>

                <View style={styles.cardTextStyle}>
                    <View style={{ flex: 2 }}>
                        <Text style={styles.textHeader}>ไปเที่ยวเชียงราย</Text>
                        <Text>วันที่เริ่ม : {props.date}</Text>
                        <Text>ระยะเวลา {props.duration} วัน</Text>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-around' }}>
                        <Button color="green" name="View Detail"
                            onPress={() => props.navigation.navigate('PlanDetail')} />
                        <Button name="Leave"
                            onPress={() => props.navigation.navigate('PlanDetail')} />
                    </View>

                </View>

            </View>
        </View>
    );
}
const styles = {
    containerStyle: {
        height: 150,
        borderColor: '#ddd',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        elevation: 2,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10,
        borderRadius: 10,
    },
    container: {
        flex: 1,
        flexDirection: 'row',
        padding: 5,
        borderRadius: 10,
        backgroundColor: '#fff',
        justifyContent: 'flex-start',
        position: 'relative'
    },
    imageStyle: {
        flex: 2,
        width: 100,
        height: 100,
        borderRadius: 10,
        margin: 10
    },
    cardTextStyle: {
        flex: 3,
        justifyContent: 'space-around',
        margin: 10,
    },
    textHeader: {
        fontSize: 20,
        fontWeight: 'bold',
    }

}

export default MyPlanCard;