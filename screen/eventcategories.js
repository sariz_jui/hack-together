import React, { Component } from "react";
import { FaBeer } from 'react-icons/';
import { View, Text, StyleSheet, Button, ImageBackground, ScrollView, TouchableOpacity } from "react-native";
import Header from '../src/component/Header';
import FooterBar from "../src/component/FooterBar";

class eventcategories extends Component {

    static navigationOptions = {
        header: null
    }

    render(props) {
        return (
            <View style={{ flex: 1 }}>
                <Header name="กลุ่มท่องเที่ยวแนะนำ" navigation={this.props.navigation} />
                <ScrollView style={{ flex: 1 }}>
                    <View style={styles.containerStyle}>
                        <TouchableOpacity style={styles.container} onPress={() => this.props.navigation.navigate('SaiBunScreen')}>
                            <ImageBackground source={require('../src/images/type1.jpg')} style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={styles.textCategory}>สายบุญ</Text>
                            </ImageBackground>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.containerStyle}>
                        <TouchableOpacity style={styles.container} onPress={() => this.props.navigation.navigate('SaiLuiScreen')}>
                            <ImageBackground source={require('../src/images/type2.jpg')} style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={styles.textCategory}>สายลุย</Text>
                            </ImageBackground>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.containerStyle}>
                        <TouchableOpacity style={styles.container} onPress={() => this.props.navigation.navigate('SaiChewScreen')}>
                            <ImageBackground source={require('../src/images/type3.jpg')} style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={styles.textCategory}>สายชิว</Text>
                            </ImageBackground>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.containerStyle}>
                        <TouchableOpacity style={styles.container} onPress={() => this.props.navigation.navigate('SaiShopScreen')}>
                            <ImageBackground source={require('../src/images/type4.jpg')} style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={styles.textCategory}>สายช็อบ</Text>
                            </ImageBackground>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.containerStyle}>
                        <TouchableOpacity style={styles.container} onPress={() => this.props.navigation.navigate('SaiMuenScreen')}>
                            <ImageBackground source={require('../src/images/type5.jpg')} style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={styles.textCategory}> สายมึน</Text>
                            </ImageBackground>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                <FooterBar navigation={this.props.navigation} />
            </View >

        );
    }

}
export default eventcategories;

const styles = StyleSheet.create({
    containerStyle: {
        borderWidth: 1,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        elevation: 2,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10
    },
    container: {
        height: 160,
        borderBottomWidth: 1,
        backgroundColor: '#fff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        position: 'relative'
    },
    textCategory: {
        fontSize: 50,
        color: 'rgba(255, 255, 255,.80)',
        fontWeight: 'bold'
    }
});
