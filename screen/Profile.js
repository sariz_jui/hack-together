import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
} from "react-native";
import MyPlanCard from '../src/component/MyPlanCard';

class profile extends Component {

    static navigationOptions = {
        title: 'โปรไฟล์ของฉัน',
        headerStyle: {
            backgroundColor: '#0878a4',
        },
    };

    render() {
        return (
            <View style={styles.container}>
                <View style={{ flex: 1 }}>
                    <View style={styles.headerContainer}>
                        <View style={{ backgroundColor: 'white', height: 80, width: 80, borderRadius: 40 }}>
                        </View>
                        <Text style={{ fontSize: 20, fontWeight: 'bold', margin: 10 }}>Profile 1</Text>
                    </View>

                    <View style={styles.planContainer}>
                        <Text style={{ alignSelf: 'center', padding: 5, fontSize: 15 }}>กิจกรรมของฉัน</Text>
                        <View style={{ borderBottomWidth: 2, marginLeft: 20, marginRight: 20, borderBottomColor: 'gray' }} />
                        <MyPlanCard />
                    </View>
                </View>
            </View>
        );
    }
}
export default profile;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    headerContainer: {
        height: 120,
        alignItems: 'center',
        justifyContent: 'flex-end',
        margin: 20
    },
    planContainer: {
        margin: 10,
        borderRadius: 10,
        backgroundColor: '#1ecfd6',
        flex: 1
    }

});