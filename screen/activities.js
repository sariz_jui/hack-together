import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    ImageBackground,
    ScrollView,
    Modal,
    TouchableOpacity
} from "react-native";
import firebase from "firebase";
import Header from '../src/component/Header';
import AllPlan from '../src/component/AllPlanCard';
import FooterBar from '../src/component/FooterBar';
import ListTravel from '../src/component/ListTravel';
import Card from '../src/component/Card';

class activities extends Component {
    static navigationOptions = {
        header: null
    }


    constructor(props) {
        super(props);
        this.state = {
            activity: [],
            modalVisible: false
        };
    }


    render(props) {
        return (
            <View style={{ flex: 1 }}>
                <Header name="แผนท่องเที่ยวที่น่าสนใจ" navigation={this.props.navigation} />
                <ScrollView >
                    <AllPlan navigation={this.props.navigation} />
                    <AllPlan navigation={this.props.navigation} />
                </ScrollView>
                <FooterBar navigation={this.props.navigation} />
            </View >

        );
    }

}
export default activities;

const styles = StyleSheet.create({
    containerStyle: {
        borderWidth: 1,
        borderRadius: 2,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        elevation: 2,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10
    },
    button: {
        position: "absolute",
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: "#8BBBDE",
        bottom: 100,
        zIndex: 2,
        right: 15,
        alignItems: "center",
        justifyContent: "center"
    },
    iconContainer: {
        width: 50,
        height: 50,
        alignItems: "center",
        justifyContent: "center"
    },
    container: {
        height: 200,
        borderBottomWidth: 1,
        padding: 5,
        backgroundColor: '#fff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        position: 'relative'
    }
});
