import React, { Component } from "react";
import { FaBeer } from 'react-icons/';
import { View, Text, StyleSheet, Button, ImageBackground, ScrollView, TouchableOpacity } from "react-native";
import Header from '../src/component/Header';
import FooterBar from "../src/component/FooterBar";

class saishop extends Component {

    static navigationOptions = {
        title: 'สายช็อบ',
        headerStyle: {
            backgroundColor: '#0878a4',
        },
    };
    render(props) {
        return (
            <View style={{ flex: 1 }}>
                <ScrollView style={{ flex: 1 }}>
                    <View style={styles.containerStyle}>
                        <ImageBackground source={require('../src/images/saishop/01.jpg')} style={styles.container}>
                            <View Style={{ flex: 2 }}>
                            </View>
                            <View style={{ flex: 1, justifycontent: 'space-around', backgroundColor: 'rgba(0,0,0,0.5)', flexDirection: 'row' }}>
                                <Text style={styles.textCategory}>ถนนคนเดินเชียงราย</Text>
                                <View style={{ flex: 1, margin: 15 }}>
                                    <Button title='สร้างกิจกรรม' onPress={() => this.props.navigation.navigate('CreateActivityScreen')} />
                                </View>
                            </View>
                        </ImageBackground>
                    </View>
                    <View style={styles.containerStyle}>
                        <ImageBackground source={require('../src/images/saishop/02.jpg')} style={styles.container}>
                            <View Style={{ flex: 2 }}>
                            </View>
                            <View style={{ flex: 1, justifycontent: 'space-around', backgroundColor: 'rgba(0,0,0,0.5)', flexDirection: 'row' }}>
                                <Text style={styles.textCategory}>ตลาดแม่สาย</Text>
                                <View style={{ flex: 1, margin: 15 }}>
                                    <Button title='สร้างกิจกรรม' onPress={() => this.props.navigation.navigate('CreateActivityScreen')} />
                                </View>
                            </View>
                        </ImageBackground>
                    </View>
                    <View style={styles.containerStyle}>
                        <ImageBackground source={require('../src/images/saishop/03.jpg')} style={styles.container}>
                            <View Style={{ flex: 2 }}>
                            </View>
                            <View style={{ flex: 1, justifycontent: 'space-around', backgroundColor: 'rgba(0,0,0,0.5)', flexDirection: 'row' }}>
                                <Text style={styles.textCategory}>กาดหน้ามอ</Text>
                                <View style={{ flex: 1, margin: 15 }}>
                                    <Button title='สร้างกิจกรรม' onPress={() => this.props.navigation.navigate('CreateActivityScreen')} />
                                </View>
                            </View>
                        </ImageBackground>
                    </View>
                    <View style={styles.containerStyle}>
                        <ImageBackground source={require('../src/images/saishop/04.jpg')} style={styles.container}>
                            <View Style={{ flex: 2 }}>
                            </View>
                            <View style={{ flex: 1, justifycontent: 'space-around', backgroundColor: 'rgba(0,0,0,0.5)', flexDirection: 'row' }}>
                                <Text style={styles.textCategory}>ถนนคนเดินท่าแพ</Text>
                                <View style={{ flex: 1, margin: 15 }}>
                                    <Button title='สร้างกิจกรรม' onPress={() => this.props.navigation.navigate('CreateActivityScreen')} />
                                </View>
                            </View>
                        </ImageBackground>
                    </View>
                    <View style={styles.containerStyle}>
                        <ImageBackground source={require('../src/images/saishop/05.jpg')} style={styles.container}>
                            <View Style={{ flex: 2 }}>
                            </View>
                            <View style={{ flex: 1, justifycontent: 'space-around', backgroundColor: 'rgba(0,0,0,0.5)', flexDirection: 'row' }}>
                                <Text style={styles.textCategory}>ตลาดวโรรส</Text>
                                <View style={{ flex: 1, margin: 15 }}>
                                    <Button title='สร้างกิจกรรม' onPress={() => this.props.navigation.navigate('CreateActivityScreen')} />
                                </View>
                            </View>
                        </ImageBackground>
                    </View>
                </ScrollView>
            </View >

        );
    }
}

const styles = StyleSheet.create({
    containerStyle: {
        borderWidth: 1,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        elevation: 2,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
        borderRadius: 5,
    },
    container: {
        height: 160,
        borderBottomWidth: 1,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'flex-end',
        flexDirection: 'row',
        borderColor: '#ddd',
        position: 'relative',
        borderRadius: 5,
    },
    textCategory: {
        flex: 1,
        fontSize: 30,
        padding: 5,
        color: 'rgba(255, 255, 255,.80)',
        fontWeight: 'bold'
    }
});

export default saishop;