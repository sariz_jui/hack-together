import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    Image,
    TouchableOpacity,
} from "react-native";
import Header from '../src/component/Header'

class mainselector extends Component {
    static navigationOptions = {
        header: null
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Header name="What's Your decision" navigation={this.props.navigation} />
                <View style={styles.container}>
                    <View style={styles.cardContainer}>
                        <TouchableOpacity style={styles.decisionStyle} onPress={() => this.props.navigation.navigate('ActivityScreen')}>
                            <Text style={styles.textChoose}>ไปเที่ยวกับเพื่อน</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.decisionStyle} onPress={() => this.props.navigation.navigate('CategoryScreen')}>
                            <Text style={styles.textChoose}>เส้นทางตัวเอง</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}
export default mainselector;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    cardContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 200,
    },
    decisionStyle: {
        height: 80,
        width: 300,
        borderRadius: 10,
        backgroundColor: 'gray',
        margin: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textChoose: {
        fontSize: 30,
        fontWeight: 'bold',
        color: 'white',
    }
});