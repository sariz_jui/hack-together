import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    FlatList,
    ScrollView,
    TouchableOpacity,
    TextInput,
} from 'react-native';
import firebase from 'firebase';
import Header from '../src/component/Header';
import Spinner from '../src/component/Spinner';
import ListTravel from '../src/component/ListTravel';
import Button from '../src/component/Button';

class createactivity extends Component {
    // constructor() {
    //     super();
    //     this.state = {
    //         place: [{
    //             truser: '',
    //             trname: '',
    //             trdescription: '',
    //             trpicture: '',

    //         }],
    //     };
    // }
    // componentDidMount() {
    //     this.fetchPlace();
    // }

    // async fetchPlace() {
    //     let data = [];
    //     await firebase
    //         .database()
    //         .ref("place")
    //         .on("value", snapshot => {
    //             snapshot.forEach(snap => {
    //                 const value = snap.val();
    //                 value.id = snap.key;
    //                 data.push(value);
    //             });
    //             data.reverse();
    //             this.setState({ place: data });
    //         });
    // }

    // renderRow(place) {
    //     return <ListTravel place={place} />;
    // }


    static navigationOptions = {
        title: 'สร้างแผนการท่องเที่ยว',
        headerStyle: {
            backgroundColor: '#0878a4',
        },
    };

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.container}>
                    <View style={{ flex: 1, margin: 10 }}                    >
                        <Text style={{ fontSize: 24, alignSelf: 'center', fontWeight: "bold" }}>
                            สร้างกิจกรรม
                        </Text>
                        <View style={styles.textSyle}>
                            <Text style={styles.innerText}>ชื่อแผนท่องเที่ยว</Text>
                            <TextInput style={{ flex: 1 }}></TextInput>
                        </View>
                        <View style={styles.textSyle}>
                            <Text style={styles.innerText}>สถานที่ๆจะไป</Text>
                            <TextInput style={{ flex: 1 }}></TextInput>
                        </View>
                        <View style={styles.textSyle}>
                            <Text style={styles.innerText}>วันที่จะไป</Text>
                            <TextInput style={{ flex: 1 }}></TextInput>
                        </View>
                        <View style={styles.textSyle}>
                            <Text style={styles.innerText}>ไปทั้งหมด</Text>
                            <TextInput style={{ flex: 1 }}></TextInput>
                            <Text style={styles.innerText}>วัน</Text>
                        </View>
                        <View style={{ height: 40, margin: 3 }}>
                            <Button name="ยืนยัน" color="green"/>
                        </View>

                    </View >

                </View >
            </View >
        );
    }
}
export default createactivity;

const styles = {
    button: {
        position: "absolute",
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: "#8BBBDE",
        bottom: 25,
        zIndex: 2,
        right: 15,
        alignItems: "center",
        justifyContent: "center"
    },
    iconContainer: {
        width: 50,
        height: 50,
        alignItems: "center",
        justifyContent: "center"
    },
    container: {
        flex: 1,
    },
    cardContainer: {
        backgroundColor: 'gray',
        height: 200,
    },
    textSyle: {
        borderWidth: 1,
        alignItems: 'center',
        flexDirection: 'row',
        margin: 5,
        borderRadius: 5,
    },
    innerText: {
        padding: 10
    }
};