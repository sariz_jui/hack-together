import React, { Component } from 'react';
import { createBottomTabNavigator, createStackNavigator } from 'react-navigation';
import Activities from './screen/activities';
import CreateActivities from './screen/createactivity';
import Profile from './screen/profile';
import Mainselector from './screen/mainselector';
import EventCategories from './screen/eventcategories';

//5 ways
import SaiBun from './screen/saibun';
import SaiLui from './screen/sailui';
import SaiChew from './screen/saichew';
import SaiShop from './screen/saishop';
import SaiMuen from './screen/saimuen';


import firebase from 'firebase';

const SelectorStack = createStackNavigator(
  {
    SelectorScreen: { screen: Mainselector },
    ProfileScreen: { screen: Profile },
  },
  {
  }
)

const ActivityStack = createStackNavigator(
  {
    ActivityScreen: { screen: Activities },
    CreateActivityScreen: { screen: CreateActivities }
  },
  {

  }
)

const CategoryStack = createStackNavigator(
  {
    CategoryScreen: { screen: EventCategories },
    SaiBunScreen: { screen: SaiBun },
    SaiLuiScreen: { screen: SaiLui },
    SaiChewScreen: { screen: SaiChew },
    SaiShopScreen: { screen: SaiShop },
    SaiMuenScreen: { screen: SaiMuen }
  },
  {

  }
)

const RootStack = createBottomTabNavigator({

  SelectorScreen: { screen: SelectorStack },
  ActivityScreen: { screen: ActivityStack },
  CategoryScreen: { screen: CategoryStack },



},
  {
    initialRouteName: 'SelectorScreen',
    navigationOptions: {
      tabBarVisible: false
    }
  });

class App extends Component {

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    firebase.initializeApp({
      apiKey: 'AIzaSyCrR8LHbk1V3h1iiIo52yv6wb3ZaF6FUow',
      authDomain: 'traveltogether-a3314.firebaseapp.com',
      databaseURL: 'https://traveltogether-a3314.firebaseio.com',
      projectId: 'traveltogether-a3314',
      storageBucket: 'traveltogether-a3314.appspot.com',
      messagingSenderId: '535026553600'
    });


  }

  render() {
    return (
      <RootStack />
    );
  }
}

export default App;




